
const path = require('path'), // Package allowing interaction with local filesystem & combining paths. Packaged with Node
      HtmlWebpackPlugin = require('html-webpack-plugin'),
      webpack = require('webpack');

let config = {
   entry: path.resolve(__dirname, 'app', 'index.jsx'),
   output: {
      path: path.resolve(__dirname, 'output'),
      filename: 'bundle.js',
      publicPath: '/' // ./
   },
   resolve: {
      extensions: ['.js', '.jsx']
   },
   module: {
      rules: [
         {
             test: /\.js(?:x?)$/,
             use: {
                loader: 'babel-loader',
                options: {
                    presets: ['react', 'env'] 
                },
                // exclude: [ path.resolve(__dirname, 'node_modules') ],
             },
             exclude: [ path.resolve(__dirname, 'node_modules') ],
         },
         {
            test: /\.(scss)$/,
            use: [{
              loader: 'style-loader', // inject CSS to page
            }, {
              loader: 'css-loader', // translates CSS into CommonJS modules
            }, {
              loader: 'postcss-loader', // Run post css actions
              options: {
                plugins: function () { // post css plugins, can be exported to postcss.config.js
                  return [
                    require('precss'),
                    require('autoprefixer')
                  ];
                }
              }
            }, {
              loader: 'sass-loader' // compiles Sass to CSS
            }]
          },
         {
             test: /\.css$/,
            //  use: 'style-loader!css-loader', // '!' chains loaders to use, in that order, the same as array notation
             use: ['style-loader','css-loader'],
             exclude: path.resolve(__dirname, 'node_modules')
             
         },
         {
             test: /\.(png|jpg)$/,
             use: { loader:'file-loader' }
         }
        //  {
        //      test: /\.(png|jpg|jpeg|gif|svg|woff|woff2|ttf|eot)(\?.*$|$)/,
        //      loader: 'file-loader'
        //  }
      ]
   },
   devServer: {
      contentBase: '/home/ubuntu/workspace/app',
       historyApiFallback: true
   },
   // This will create a new HTML file within the dist folder that will include the output script ('index_bundle.js')
    plugins: [new HtmlWebpackPlugin({
        template: 'app/index.html'
    })]
}; //config
    
if (process.env.NODE_ENV === 'production') {
    config.plugins.push(
        new webpack.DefinePlugin({
            'process.env': {
                'NODE_ENV': JSON.stringify(process.env.NODE_ENV)
            }
        }),
        new webpack.optimize.UglifyJsPlugin()
    );
}

module.exports = config;