const data = [{
	"basics": {
		"name": "Brad Sullivan",
		"label": "Experienced IT support professional and student/hobbyist web developer",
		"picture": "",
		"email": "",
		"phone": "",
		"website": "http://bradsullivan.x10host.com/",
		"summary": "My professional background is in helpdesk and customer service. I’m looking to transition into developer role and I have taken to studying and practicing with projects in my free time to gain experience with different technologies and practices.",
		"location": {
			"address": "",
			"postalCode": "",
			"city": "Fairfax",
			"countryCode": "US",
			"region": "Virginia"
		}
	},
	"work": [{
			"company": "George Mason University",
			"location": "Fairfax, VA",
			"position": "Support Center Specialist",
			"website": "https://www.gmu.edu",
			"startDate": "June 2014",
			"endDate": "Present",
			"summary": "Provide information and support through mixed delivery methods to a diverse population of users.",
			"highlights": [
				"Use various ticketing systems including SDE and EasyVista to track issues and document work.",
				"Use Bitbucket to define requirements, track issues and updates, and maintain project notes.",
				"Utilize SSH and SFTP to check permissions and interact with remote file locations",
				{
					"main": "Maintain and further develop website for Support Center use using PHP, JavaScript, and MySQL.",
					"sub": [
						"Interact with an LDAP host to gather and present user account information.",
						"Provide a common look with different options based on user role.",
						"Bi-directional updates between database and user interface via recurring AJAX requests.",
						"Downloadable CSV log files through PHP from real-time database records."
					]
				}
			]
		},
		{
			"company": "CGI Federal & URS for EPA",
			"location": "Chantilly, VA",
			"position": "Help Desk Analyst",
			"website": "",
			"startDate": "January 2013",
			"endDate": "June 2014",
			"summary": "",
			"highlights": []
		},
		{
			"company": "Unisys (Insight Global staffing) for FDIC",
			"location": "Arlington, VA",
			"position": "Help Desk Analyst",
			"website": "",
			"startDate": "August 2012",
			"endDate": "January 2013",
			"summary": "",
			"highlights": []
		}
	],
	"projects": [{
			"title": "Fictitious bike club website",
			"description": "Created wireframes using Visio. Created site using Dreamweaver, Flash, Photoshop, and other code editors. Integrated pre-packaged code to generate site menus, include a calendar, and a weather widget. Checked for accessibility using online tools such as WAVE."
		},
		{
			"title": "Current weather viewer",
			"description": "Geolocation API, weather API, Axios’ implementation of JavaScript promises. Allows toggling of temperature units. Client-side checks limit frequency of AJAX requests, so as not to overload the server or cap out the use of the free weather API plan."
		},
		{
			"title": "Wikipedia viewer",
			"description": "Wikipedia search API and JQuery’s autocomplete function for search suggestions."
		},
		{
			"title": "Web calculator",
			"description": "JQuery, Bootstrap, and Less CSS pre-processing."
		},
		{
			"title": "Pomodoro clock",
			"description": "Notification API, CSS transformations, media queries."
		},
		{
			"title": "Fictitious bike club website",
			"description": "Computer AI for single-player option. ReactJS."
		},
		{
			"title": "Simon game",
			"description": "Various CSS styling and positioning, audio rendering."
		},
		{
			"title": "Recipe box",
			"description": "React, Bootstrap modals, SCSS, localStorage."
		},
		{
			"title": "EDX course",
			"description": "Utilized Nodejs and npm package to convert values from a CSV file to JSON using a JSON schema, running tests through Mocha and Chai."
		}
	],
	"education": [{
			"institution": "George Mason University",
			"area": "Applied Information Technology",
			"studyType": "Coursework",
			"year": "2015",
			"credits": "12"
		},
		{
			"institution": "Northern Virginia Community College",
			"area": "Application Development, Web Design & Development",
			"studyType": "Coursework",
			"year": "2011",
			"credits": "30"
		},
		{
			"institution": "James Madison University",
			"area": "Integrated Science and Technology",
			"studyType": "B.S.",
			"year": "2009"
		}
	],
	"awards": [{
		"title": "Nominee for the HDI Analyst of the Year Award",
		"date": "2017",
		"awarder": "GMU/HDI",
		"summary": ""
	}],
	"websites": [{
		"name": "Personal Website",
		"publisher": "",
		"releaseDate": "",
		"website": "http://bradsullivan.x10host.com/",
		"summary": "Personal website with portfolio of work, resume, and other items and links"
	}],
	"skills": [{
		"name": "Web Development",
		"level": "Junior",
		"keywords": [
			"HTML", "CSS/SCSS/Less", "React", "JQuery", "Bootstrap", "JavaScript & ES6", "localStorage", "Flexbox", "Google Fonts", "Font Awesome", "API consumption", "AJAX", "Promises", "JSON", "SSH", "SFTP", "Git", "Browser developer tools", "NPM", "Webpack", "Yarn"
		],
		"bullets": [
			"HTML, CSS/SCSS/Less, React, JQuery, Bootstrap, JavaScript & ES6, localStorage, Flexbox, Google Fonts, Font Awesome, API consumption, AJAX, Promises, JSON, SSH, SFTP",
			"Git version control – BitBucket or GitHub",
			"JavaScript task automation tools (NPM, Webpack, Yarn)",
			"Browser developer tools for inspecting page details, local storage, and network traffic"
		]
	}]
}];

console.log(data);