import React from 'react';
import ReactDOM from 'react-dom';

import '../styles/project.scss';

const Project = (props) => {
    
    const { src, alt, title, description } = props;
    
    if (src) {
        return (
            <div className="project">
                <div className="project-img"><img src={src} alt={alt} /></div>
                <div className="project-info">
                    <h3>{title}</h3>
                    <p>{description}</p>
                </div>
            </div>
        );
    } else {
        return <div>Loading..</div>;
    }
    
}; // /Project

export default Project;