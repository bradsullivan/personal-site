import React from 'react';
import ReactDOM from 'react-dom';

import '../styles/nav.scss';

const Nav = (props) => {
    
    return (
        <div className="nav">
            <ul>
                <li>Top</li>
                <li>Portfolio</li>
                <li>Resume</li>
                <li>Contact</li>
            </ul>
        </div>
    );
};



export default Nav;