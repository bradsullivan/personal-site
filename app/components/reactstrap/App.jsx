import React from 'react';
import ReactDOM from 'react-dom';
import Header from './Header.jsx';
import Nav from './Nav.jsx';
import Portfolio from './Portfolio.jsx';
// import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
import { Container, Row, Col } from 'reactstrap';


import '../styles/app.scss';

const App = () => {
    
    return (
        <div className="app">
            <Header />
            <Portfolio />
        </div>
    );
    
};

export default App;