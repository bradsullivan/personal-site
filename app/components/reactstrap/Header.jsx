import React from 'react';
import ReactDOM from 'react-dom';
import Nav from './Nav';

import '../styles/header.scss';
// import '../styles/app.scss';

const Header = () => {
    
    return (
        <header>
            <div className='flex-col'>
                <h1>Brad Sullivan</h1>
                <Nav />
            </div>
            <div class="img-flex"><img src={require('../images/me-cropped.jpg')} alt='my face' /></div>
        </header>
    );
    
};

export default Header;