import React from 'react';
import ReactDOM from 'react-dom';
import Project from './Project.jsx'

import '../styles/portfolio.scss';

import imgNotFound from '../images/question-mark.jpg';
import imgBike from '../images/bikeclub-screenshot.jpg';
import imgNMNH from '../images/nmnh-gis-screenshot.jpg';
import imgNMNH2 from '../images/nmnh-gis-screenshot2.png';
import imgNMNH3 from '../images/nmnh-gis-screenshot3.jpg';
import imgQuote1 from '../images/random-quote.png';
import imgQuote2 from '../images/random-quote2.png';
import imgCurrentWeather from '../images/current-weather.png';
import imgWikipediaViewer from '../images/wikipedia-viewer.png';
import imgTwitchTVViewer from '../images/twitch-tv-viewer.png';
import imgCalculator from '../images/calculator.png';
import imgPomodoroClock from '../images/pomodoro-clock.png';
import imgTicTacToe from '../images/tic-tac-toe.png';
import imgSimon from '../images/simon.png';
import imgMarkdown from '../images/markdown-previewer.png';
import imgLeaderboard from '../images/camper-leaderboard.png';
import imgRecipeBox from '../images/recipe-box.png';
import imgRecipeBoxAdd from '../images/recipe-box-add.png';
import imgRecipeBoxEdit from '../images/recipe-box-edit.png';
import imgGameOfLife from '../images/game-of-life.png';


const Portfolio = (props) => {
    
    const projects = [
        {
            title: "Bike Club",
            description: "Project from web design class at GMU based on a fictional Fairfax City Bike Club website. Incporporated custom pages with an integrated Google Calendar and Flash video. Weather widgit and responsive menu developed elsewhere and used in this project.",
            technologies: ['HTML','CSS', 'Flash'],
            // src: "../images/bikeclub-screenshot.jpg",
            src: imgBike,
            url: "http://mason.gmu.edu/~wsulliv4/it213/project5/"
        },
        {
            title: "Natural Museum of Natural History (NMNH) - GIS",
            description: "Web project developed on top of NMNH Dreamweaver web templates.",
            technologies: ['HTML','CSS'],
            // src: "../images/nmnh-gis-screenshot.jpg",
            src: [imgNMNH, imgNMNH2, imgNMNH3],
            url: "https://naturalhistory.si.edu/gismaps/index.html"
        },
        {
            title: "Random Quote Machine Complete",
            description: "Free Code Camp project. Random quotes pulled through JSONP API. Interacts with Twitter API to draft a tweet with the quote.",
            technologies: ["JQuery", "Bootstrap","Twitter API"],
            // src: ["../images/random-quote.png","../images/random-quote2.png"],
            src: [imgQuote1, imgQuote2],
            url: "https://codepen.io/bridic/full/dZbxjo/",
            fcc: "https://www.freecodecamp.org/challenges/build-a-random-quote-machine"
        },
        {
            title: "Current Weather Viewer",
            description: "Free Code Camp project. Displays the current weather, along with matching icons and backgrounds.",
            technologies: ['JQuery', 'Object literal code organization','Bootstrap','Geolocation API', 'apixu.com weather API','axios'],
            // src: "../images/current-weather.png",
            src: imgCurrentWeather,
            url: "https://codepen.io/bridic/full/RLzWLW",
            fcc: "https://www.freecodecamp.org/challenges/show-the-local-weather"
        },
        {
            title: "Wikipedia Viewer",
            description: "Free Code Camp project. Low profile Wikipedia search with autocomplete search suggestions provided through JQuery",
            technologies: ['JQuery','Bootstrap','Wikipedia API'],
            // src: "../images/wikipedia-viewer.png",
            src: imgWikipediaViewer,
            url: "https://codepen.io/bridic/full/gmRRyq",
            fcc: "https://www.freecodecamp.org/challenges/build-a-wikipedia-viewer"
        },
        //Twitch TV needs work to fulfill full functionality
        {
            title: "Twitch TV Viewer",
            description: "Free Code Camp project. Lists (pre-defined) Twitch users and their online/offline/streaming status. Clicking the account name links to the active stream if a user is streaming.",
            technologies: ['JQuery','Bootstrap','Free Code Camp Twitch API Pass-through','axios'],
            // src: "../images/twitch-tv-viewer.png",
            src: imgTwitchTVViewer,
            url: "https://codepen.io/bridic/full/Opjbdj",
            fcc: "https://www.freecodecamp.org/challenges/use-the-twitchtv-json-api"
        },
        {
            title: "Calculator",
            description: "Free Code Camp project. Functioning calculator. Utilizes Flex CSS positioning, BigJS for processing numbers with decimal rounding.",
            technologies: ['JQuery','Bootstrap', 'Less CSS Pre-processing','Flex CSS layout','Big JS'],
            // src: "../images/calculator.png",
            src: imgCalculator,
            url: "https://codepen.io/bridic/full/zwYQoE/",
            fcc: "https://www.freecodecamp.org/challenges/build-a-javascript-calculator"
        },
        {
            title: "Pomodoro Clock",
            description: "Free Code Camp project. Pomodoro clock. Settings for work & break time. Countdown of time left in each state, with visual notification on the page as well as a notification on the computer screen, given the use of notifications was accepted by the visitor/user of the page.",
            technologies: ['JQuery', "Notification web API", "Google Font imports", "Less CSS Pre-processing", "CSS transform", "CSS media query",'Flex CSS layout'],
            // src: "../images/pomodoro-clock.png",
            src: imgPomodoroClock,
            url: "https://codepen.io/bridic/full/aWvzpe/",
            fcc: "https://www.freecodecamp.org/challenges/build-a-pomodoro-clock"
        },
        {
            // Needs fix for clicking already used spot in 1 player mode
            title: "Tic Tac Toe",
            description: "Free Code Camp project. Project base originally coded along with React tutorial, then added upon for additional functionality: https://facebook.github.io/react/tutorial/tutorial.html.",
            technologies: ["React","Less CSS Pre-processing"],
            // src: "../images/tic-tac-toe.png",
            src: imgTicTacToe,
            url: "https://codepen.io/bridic/full/jmVVvY",
            fcc: "https://www.freecodecamp.org/challenges/build-a-tic-tac-toe-game"
        },
        {
            title: "Simon Game",
            description: "Free Code Camp project. Simon Says game with 4 colored buttons. Follow the visual and audio patterns set by the game. Toggle strict mode on to restart game on failure versus restarting the current level on failure.",
            technologies: ["JQuery","Less CSS Pre-processing"],
            // src: "../images/simon.png",
            src: imgSimon,
            url: "https://codepen.io/bridic/full/qmpPmz",
            fcc: "https://www.freecodecamp.org/challenges/build-a-simon-game"
        },
        {
            title: "Markdown Previewer",
            description: "Free Code Camp project. Type Markdown. Get HTML",
            technologies: ["JQuery","Less CSS Pre-processing", "Bootstrap"],
            // src: "../images/markdown-previewer.png",
            src: imgMarkdown,
            url: "https://codepen.io/bridic/full/pPYOgX",
            fcc: "https://www.freecodecamp.org/challenges/build-a-markdown-previewer"
        },
        {
            // Incomplete
            title: "Camper Leaderboard",
            description: "Free Code Camp project. Table of top Free Code Camp contributors. Sortable on multiple columns",
            technologies: ["JQuery","Sass", "React"],
            // src: "../images/camper-leaderboard.png",
            src: imgLeaderboard,
            url: "https://codepen.io/bridic/full/KqKdxV",
            fcc: "https://www.freecodecamp.org/challenges/build-a-camper-leaderboard"
        },
        {
            // Incomplete
            title: "Recipe Box",
            description: "Free Code Camp project. Store recipes through localStorage. Sync with React state. Allows for storage and editing of recipes with titles and ingredients.",
            technologies: ["JQuery","SCSS", 'React', 'Bootstrap', 'Web Storage - localStorage'],
            // src: "../images/recipe-box.png",
            src: [imgRecipeBox, imgRecipeBoxAdd, imgRecipeBoxEdit],
            url: "https://codepen.io/bridic/full/XgRLYW",
            fcc: "https://www.freecodecamp.org/challenges/build-a-recipe-box"
        },
        {
            title: "Game of Life",
            description: "Free Code Camp project. Board initialized with randomly placed active cells. Cells are born and die based on Conway's Game of Life rules. Features include configurable options for board size and game speed, a count of how many generations have passed, and start/pause/stop buttons.",
            technologies: ["JQuery","React","SCSS", "Flex CSS layout"],
            // src: "../images/game-of-life.png",
            src: imgGameOfLife,
            url: "https://codepen.io/bridic/full/yXKRQw",
            fcc: "https://www.freecodecamp.org/challenges/build-the-game-of-life"
        }
    ]; // projects[]
    
    return (
        <div className="portfolio">
            <h1 id="portfolio">Portfolio</h1>
            {
                projects.map( (e,i) => {
                    // console.log(e);
                    return <Project
                        key={`project${i}`}
                        src={ (Array.isArray(e.src) ? e.src[0] : e.src) || imgNotFound }
                        alt={`${e.title} screenshot`}
                        title={e.title}
                        plink={e.url}
                        fcclink={e.fcc ? e.fcc : null}
                        description={e.description} />;
                })
            }
        </div>
    );
    
};

export default Portfolio;