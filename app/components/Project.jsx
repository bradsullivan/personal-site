import React from 'react';
import ReactDOM from 'react-dom';

import '../styles/project.scss';

const Project = (props) => {
    
    const { src, alt, title, description, plink, fcclink } = props;
    // console.log(props.fcclink);
    
    if (src) {
        return (
            <div className="project">
                <div className="project-main">
                    <div className="project-img"><img src={src} alt={alt} /></div>
                    <div className="project-info">
                        <h3>{title}</h3>
                        <p>{description}</p>
                    </div>
                </div>
                <div className="project-footer">
                    <div className="project-links">
                        <div className="project-link">
                            <a href={plink}><i className="fa fa-link" aria-hidden="true"></i> Project Link</a>
                        </div>
                        {
                        fcclink != null &&
                        <div className="project-fcc-link">
                            <a href={fcclink}>
                                <i className="fa fa-link" aria-hidden="true"></i> Free Code Camp project description
                            </a>
                        </div>
                        }
                        <a href="#"><i className="fa fa-arrow-up" aria-hidden="true"></i> Top</a>
                    </div>
                </div>
            </div>
        );
    } else {
        return <div>Loading..</div>;
    }
    
}; // /Project

export default Project;