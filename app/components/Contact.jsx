import React from 'react';

import '../styles/contact.scss';

const Contact = (props) => {
    
    
    return (
        <div id="contact" className="contact">
            <div>
                <p>Created by Brad Sullivan with React & Webpack</p>
            </div>
            <div className='contact-links'>
                <a href="https://codepen.io/bridic/"><i className="fa fa-codepen" aria-hidden="true"></i></a>
                <a href="https://www.freecodecamp.com/youcandoit"><i className="fa fa-free-code-camp" aria-hidden="true"></i></a>
                <a href="https://github.com/bradsullivan"><i className="fa fa-github" aria-hidden="true"></i></a>
                <a href="https://bitbucket.org/bradsullivan/"><i className="fa fa-bitbucket" aria-hidden="true"></i></a>
                
                <a href="https://www.linkedin.com/in/brad-sullivan-b1072a8/"><i className="fa fa-linkedin" aria-hidden="true"></i></a>
            </div>
        </div>
    );
};

export default Contact;