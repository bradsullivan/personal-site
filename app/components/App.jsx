import React from 'react';
import Header from './Header.jsx';
import Portfolio from './Portfolio.jsx';
import About from './About.jsx';
import Resume from './Resume.jsx';
import Contact from './Contact.jsx';
// import { BrowserRouter as Router, Route, Link } from 'react-router-dom';

import '../styles/app.scss';

const App = () => {
    
    return (
        <div className="app">
            <Header />
            <Portfolio />
            <Resume />
            <Contact />
        </div>
    );
    
};

export default App;