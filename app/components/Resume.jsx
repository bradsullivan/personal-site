import React from 'react';
import resume from "../assets/resume.json";
// import axios from 'axios';

import '../styles/resume.scss';

console.log(resume);

const JobTitle = ({position, company, location, startDate, endDate}) => (
    <p><u>{position} - {company}</u>,{location}, {startDate} - {endDate}</p>
);

const JobHighlight = ({highlight}) => {
    console.log("passed highlight: ",highlight);
    return typeof highlight == "object" && highlight != null
            ? <li style={{listStyle:"none"}}><JobHighlights highlights={highlight}/></li>
            : <li>{highlight}</li>;
};

const JobHighlights = ({highlights}) => {
    console.log("passed highlights: ",highlights);
    
    if (Array.isArray(highlights)) {
        return <ul>
        {
            highlights.map((e,i) => {
                return <JobHighlight key={"job-highlight-"+i} highlight={e} />;
            })
        }
        </ul>;
    }
    else if (typeof highlights == "object" && highlights != null) {
        return <ul>
        {
            highlights.sub.map((e,i) => {
                return <JobHighlight key={"job-sub-highlight-"+i} highlight={e} />; 
            })
        }
        </ul>;
    }
};

const Resume = (props) => {
    return (
        <div id="resume" className="resume">
            <h3>Summary</h3>
            <p>{resume.basics.summary}</p>
            
            <h3>Jobs</h3>
            {
            resume.work.map((e,i) => (
                <section key={"resume-section-"+i}>
                    <JobTitle
                        position={e.position}
                        company={e.company} 
                        location={e.location}
                        startDate={e.startDate}
                        endDate={e.endDate} />
                    
                    <JobHighlights highlights={e.highlights} />    
                </section>
                ))
            }
        </div>
    );
};

export default Resume;

// Issue: https://stackoverflow.com/questions/45524527/webpack-error-when-importing-a-json-file