import React from 'react';

import '../styles/certifications.scss';

const Certifications = (props) => {
    
    const certifications = [
        "Free Code Camp Front End Development Program – June 2017",
        "MTA Exam 98-375: HTML5 Application Development Fundamentals – November 2015",
        "MTA Exam 98-366: Networking Fundamental – November 2015",
        "MTA Exam 98-364: Database Fundamentals – November 2015",
        "HDI Support Center Analyst – December 2014",
    ];
    
    return (
        <div id="certifications" className="certifications">
        <h3>Certifications</h3>
            <ul>
                { certifications.map( e => <li>{e}</li> ) }
            </ul>
        </div>
    );
};

export default Certifications;