import React from 'react';

// import '../styles/affix.scss';

const Affix = (props) => {
    const children = props.children;
    // count = React.Children.count(children);
        
    
    return (
        <div id="affix" className="affix">
            {children}
        </div>
    );
};

export default Affix;