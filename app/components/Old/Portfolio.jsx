import React from 'react';
import ReactDOM from 'react-dom';
import Project from './Project.jsx'

import '../styles/portfolio.scss';

const Portfolio = () => {
    
    return (
        <div className="portfolio">
            <Project
            src={require("../images/bikeclub-screenshot.jpg")}
            alt="Bike Club Screenshot"
            title="Bike Club"
            description="Project from web design class at GMU based on a fictional Fairfax City Bike Club website. Incporporated custom pages with an integrated Google Calendar and Flash video. Weather widgit and responsive menu developed elsewhere and used in this project." />
        </div>
    );
    
};

export default Portfolio;