import React from 'react';
import ReactDOM from 'react-dom';
import Header from './Header.jsx';
import Nav from './Nav.jsx';
import Portfolio from './Portfolio.jsx';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';


import '../styles/app.scss';

const App = () => {
    
    return (
        <Router>
            <div className="app">
                <Header />
                <Portfolio />
            </div>
        </Router>
    );
    
};

export default App;