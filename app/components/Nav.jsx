import React from 'react';

import '../styles/nav.scss';

const Nav = (props) => {
    
    return (
        <nav className="nav">
            <ul>
                <li><a href="#">Top</a></li>
                <li><a href="#portfolio">Portfolio</a></li>
                <li><a href="#resume">Resume</a></li>
                <li><a href="#contact">Contact</a></li>
            </ul>
        </nav>
    );
};



export default Nav;




/*
import React from 'react';
import ReactDOM from 'react-dom';

import '../styles/nav.scss';

const Nav = (props) => {
    
    return (
        <nav className="nav">
            <ul>
                <li><a href="#">Top</a></li>
                <li><a href="#portfolio">Portfolio</a></li>
                <li>Resume</li>
                <li>Contact</li>
            </ul>
        </nav>
    );
};



export default Nav;
*/