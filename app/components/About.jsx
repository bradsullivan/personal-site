import React from 'react';
import Certifications from './Certifications.jsx';

// import '../styles/about.scss';

const About = (props) => {
    
    
    return (
        <div id="about" className="about">
            <Certifications />
        </div>
    );
};

export default About;