import $ from "jquery";

const elementPosition = $('nav').offset();

$(window).scroll(function(){
        if($(window).scrollTop() > elementPosition.top){
              $('nav').css('position','fixed').css('top','0');
        } else {
            $('nav').css('position','inherit');
        }    
});